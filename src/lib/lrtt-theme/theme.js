import { colors } from './colors';

const fonts = {
  headerFontFamily: `'Montserrat', sans-serif`,
  textFontFamily: `'Lato', sans-serif`
};

export const lrttTheme = {
  fonts,
  colors,
  text: {
    primary: colors.blackGrey,
    secondary: colors.mediumGrey,
    accent: colors.lrttBlue
  },
  background: {
    primary: colors.white,
    secondary: colors.offWhite
  },
  button: {
    primary: {
      color: colors.white,
      background: colors.lrttBlue,
      selectors: {
        disabled: {
          color: colors.mediumGrey,
          background: colors.disabledBackground
        }
      },
      unselected: {
        color: colors.blackGrey,
        background: colors.white
      },
      hover: {
        background: colors.lrttDarkBlue
      },
      buttonBarHover: {
        background: colors.subtleBlue
      },
      buttonEnabled: {
        background: colors.lrttBlue,
        color: colors.white
      }
    }
  },
  checkbox: {
    disabled: colors.lrttGrey,
    unchecked: colors.lrttGrey,
    checked: colors.lrttBlue,
    hoverBackground: colors.default
  },
  tooltip: {
    color: colors.offWhite,
    background: colors.lrttGrey
  },
  badge: {
    color: colors.offWhite,
    background: colors.lrttBlue
  },
  spinner: {
    color: colors.lrttBlue
  }
};
