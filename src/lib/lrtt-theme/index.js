import React from 'react';
import { ThemeProvider } from 'emotion-theming';

import { lrttTheme } from './theme';

export { lrttTheme } from './theme';

export const WithLRTTTheme = ({ children }) => (
  <ThemeProvider theme={lrttTheme}>{children}</ThemeProvider>
);
