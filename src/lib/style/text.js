import { css } from 'emotion';
import styled from 'react-emotion';

import * as mixins from './mixins';
import { getVariant } from './utils';

export const Text = styled('span')`
  ${mixins.textFont};
  font-size: 1em;

  ${({ bold }) => (bold ? css({ fontWeight: 'bold' }) : '')};
  ${({ center }) => (center ? css({ textAlign: 'center' }) : '')} ${({
    inherit,
    theme,
    ...otherProps
  }) => (inherit ? '' : css({ color: theme.text[getVariant(otherProps)] }))};
`;

export const Label = styled(Text)``;

export const Caption = styled(Text)`
  font-size: 0.8em;
`;

export const Error = styled(Caption)`
  color: red;
  ${({ children }) => css({ opacity: children ? 1 : 0 })};
  transition: all 0.3s;
`;

const HeadingBase = styled('span')`
  ${mixins.headerFont};
  ${({ theme }) => css({ color: theme.text.primary })};
  font-weight: bold;
`;

export const Heading1 = styled(HeadingBase)`
  padding-top: 1.5em;
  font-size: 1.4em;
  text-transform: uppercase;
`;

export const Heading2 = styled(HeadingBase)`
  padding-top: 1em;
  font-size: 1.2em;
`;

export const Heading3 = styled(HeadingBase)`
  padding-top: 0.5em;
  font-size: 1em;
`;

export const TextBlock = styled(Text)`
  padding: 1.5em;
  background: white;

  ${({ theme }) => css`
    border: 1px solid rgba(0, 0, 0, 0.1);
    color: ${theme.colors.deepGrey};
  `};
`;
