import React from 'react';
import styled from 'react-emotion';
import { css } from 'emotion';
import DatePicker from 'react-date-picker';

import * as mixins from './mixins';
import { Props } from '../utils/styled-utils';
import { Error } from './text';

const ValidationDiv = styled('div')`
  display: flex;
  flex-direction: column;
`;

export const Validation = ({ error, children }) => {
  let cn = '';

  if (error) {
    cn = 'error-' + children.props.propName;
  }

  return (
    <ValidationDiv>
      {children}
      <Error color="red" style={{ marginTop: '0.3em' }} className={cn}>
        {error}
      </Error>
    </ValidationDiv>
  );
};

export const InputLike = styled('div')`
  ${mixins.textFont};
  ${mixins.inputStyle};

  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Input = styled('input')`
  ${mixins.textFont};
  ${mixins.inputStyle};

  ${({ theme }) => css`
    &:disabled {
      background: ${theme.colors.miscGrey};
      opacity: 1;
    }

    &:not(:disabled) {
      &:not(:focus) {
        &:hover {
          background: ${theme.colors.subtleBlue};
          cursor: pointer;
        }
      }

      &:focus {
        box-shadow: 0 0 3px 2px ${theme.colors.subtleBlue};
      }
    }
  `};
`;

export const TextInput = Props.withProps({ type: 'text' })(Input);

const hasDateInput = () => {
  var input = document.createElement('input');
  input.setAttribute('type', 'date');

  var notADateValue = 'not-a-date';
  input.setAttribute('value', notADateValue);

  return input.value !== notADateValue;
};

export const DateInput = props => {
  if (hasDateInput()) return <NativeDateInput {...props} />;

  const { onChange = () => {}, min, max, value, ...otherProps } = props;

  const inputProps = {
    ...otherProps,
    onChange: value => onChange({ target: { value } }),
    clearIcon: null
  };

  if (value) inputProps.value = new Date(value);
  if (min) inputProps.minDate = new Date(min);
  if (max) inputProps.maxDate = new Date(max);

  return <DatePicker {...inputProps} />;
};

export const NativeDateInput = Props.withProps({ type: 'date' })(styled(Input)`
  min-width: 95%;
  -webkit-appearance: textfield;
  -moz-appearance: textfield;
  box-sizing: border-box;
  width:100%;
`);

export const TimeInput = Props.withProps({ type: 'time' })(styled(Input)`
  min-width: 95%;
  -webkit-appearance: textfield;
  -moz-appearance: textfield;
  box-sizing: border-box;
  width: 100%
`);

export const TextArea = styled(Input)`
  padding-top: 0.3em;
  padding-bottom: 0.3em;
  height: auto;
`.withComponent('textarea');

export const Select = styled('select')`
  ${mixins.textFont};
  font-size: 1.01em;
  min-width: 3em;
  border: none;
  border-radius: 0;
  padding: 0 1em;
  outline: 1px solid rgba(0, 0, 0, 0.1);
  height: 3em;
  background: white;
  -webkit-appearance: none;

  transition: all 0.3s;

  ${({ theme }) => css`
    &:not(:focus) {
      transition: background: 0s;
      &:hover {
        background: ${theme.colors.subtleBlue};
        cursor: pointer;
      }
    }

    &:focus {
      box-shadow: 0 0 3px 2px ${theme.colors.subtleBlue};
    }
  `};
`;

const CheckboxContainer = styled('div')`
  ${mixins.textFont};
  font-size: 0.9em;
  min-width: 3em;
  padding: 0.5em 0.5em;
  background: white;
  border: none;
  border-radius: 0;
  display: flex;
  align-items: center;
  transition: all 0.3s;
  user-select: none;

  input {
    position: absolute;
    opacity: 0;
  }

  ${({ disabled, checked, theme }) =>
    disabled
      ? ''
      : css`
          &:hover {
            background: ${theme.colors.subtleBlue};
            cursor: pointer;
          }

          &:focus {
            box-shadow: 0 0 3px 2px ${theme.colors.subtleBlue};
          }
        `};
`;

export const CheckboxInput = styled('div')`
  min-width: 1.5em;
  min-height: 1.5em;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 0.7em;

  ${({ disabled, theme, checked }) => css`
    border: 2px solid
      ${disabled ? theme.colors.mediumGrey : theme.colors.lrttBlue};

    ${!checked
      ? ''
      : css`
          &:after {
            position: absolute;
            color: ${disabled
              ? theme.colors.mediumGrey
              : theme.colors.lrttBlue};

            font-family: 'Font Awesome 5 Free';
            font-weight: 900;
            content: '\\f00c';
          }
        `};
  `};
`;

export const Checkbox = ({ text, onClick, onKeyUp, id, ...props }) => (
  <CheckboxContainer
    tabIndex={0}
    onClick={onClick}
    checked={props.checked}
    disabled={props.disabled}
    onKeyUp={onKeyUp}
    id={id}
  >
    <CheckboxInput disabled={props.disabled} checked={props.checked} />
    {text}
  </CheckboxContainer>
);

const RadioContainer = styled('div')`
  ${mixins.textFont};
  font-size: 1.01em;
  min-width: 3em;
  outline: 1px solid rgba(0, 0, 0, 0.1);
  padding: 0.3em 0.5em;
  background: white;
  border: none;
  border-radius: 0;
  display: flex;
  align-items: center;
  transition: all 0.3s;
  user-select: none;

  input {
    position: absolute;
    opacity: 0;
  }

  ${({ theme }) => css`
    &:focus {
      box-shadow: 0 0 3px 2px ${theme.colors.subtleBlue};
    }
  `};

  ${({ checked, theme }) =>
    checked
      ? ''
      : css`
          &:hover {
            background: ${theme.colors.subtleBlue};
            cursor: pointer;
          }
        `};
`;

const RadioInput = styled('div')`
  position: relative;
  width: 16px;
  height: 16px;
  margin: 8px;
  border-radius: 50%;
  border: 1px solid rgba(0, 0, 0, 0.9);
  transition: all 0.3s;

  &:after {
    transition: all 0.3s;
    position: absolute;
    ${({ checked, theme }) =>
      css({ background: checked ? theme.colors.lrttBlue : 'none' })};
    border-radius: 50%;
    top: 2px;
    left: 2px;
    height: 12px;
    width: 12px;
    content: ' ';
  }
`;

export const Radio = ({ text, onClick, onKeyUp, id, ...props }) => (
  <RadioContainer
    id={id}
    tabIndex={0}
    onClick={onClick}
    onKeyUp={onKeyUp}
    checked={props.checked}
  >
    <RadioInput type="radio" {...props} />
    {text}
  </RadioContainer>
);
