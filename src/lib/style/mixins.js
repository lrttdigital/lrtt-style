import { css } from 'emotion';

export const textFont = ({ theme }) =>
  css({ fontFamily: theme.fonts.textFontFamily });

export const headerFont = ({ theme }) =>
  css({ fontFamily: theme.fonts.headerFontFamily });

export const inputStyle = `
  font-size: 1.01em;
  min-width: 3em;
  outline: 1px solid rgba(0, 0, 0, 0.1);
  height: 3em;
  padding: 0 1em;
  background: white;
  border: none;
  border-radius: 0;
  transition: all 0.3s;
  position: relative;
`;
