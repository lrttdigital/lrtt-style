import { css } from 'emotion';
import styled from 'react-emotion';

import { ButtonBase } from './button-base';
import { getVariant } from '../utils';

export const Button = styled(ButtonBase)`
  ${({ large }) => (!large ? '' : css({ fontSize: '1.3em', padding: '1em' }))}
  ${({ white }) => (!white ? '' : css({ background: '#fff', color: 'rgb(55, 160, 250)', border: '1px solid rgb(55, 160, 250)', fontSize: '1em', lineHeight: '1.44em', padding: '0.7em' }))}
  ${({ inactive }) => (!inactive ? '' : css({ background: 'lightgray', cursor: 'default' }))}


  &:enabled {
    cursor: pointer;

    ${({ theme, ...otherProps }) =>
      css({ ':hover': theme.button[getVariant(otherProps)].hover })};
`;
