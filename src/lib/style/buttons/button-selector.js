import { css } from 'emotion';
import styled from 'react-emotion';

import { ButtonBase } from './button-base';
import { getVariant } from '../utils';

export const ButtonSelector = styled('div')`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;

  @media (max-width: 768px) {
    flex-wrap: wrap;
  }
`;

ButtonSelector.Button = styled(ButtonBase)`
  margin: 0;
  border-radius: 0;
  border: 0;
  width: 100%;
  outline: 1px solid rgba(0, 0, 0, 0.1);

  @media (max-width: 768px) {
    flex: 1 0 21%;
  }

  ${({ selected, theme, ...otherProps }) => {
  const colors = theme.button[getVariant(otherProps)];

  if (selected) return css(colors);

  const { color, background } = colors.unselected;

  return css`
      ${css({ color, background })};

      &:enabled {
        cursor: pointer;

        ${css({ '&:hover': colors.buttonBarHover })};
      }
    `;
}};
`;
