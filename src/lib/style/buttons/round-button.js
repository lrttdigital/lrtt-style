import { css } from 'emotion';
import styled from 'react-emotion';

import { getVariant } from '../utils';
import { ButtonBase } from './button-base';

export const RoundButton = styled(ButtonBase)`
  font-size: 1.2em;
  width: 3.5em;
  height: 3.5em;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 0;

  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2),
    0px 6px 10px 0px rgba(0, 0, 0, 0.14), 0px 1px 18px 0px rgba(0, 0, 0, 0.12);

  &:disabled {
    opacity: 0.5;
  }

  &:enabled {
    ${({ theme, ...otherProps }) => {
      const colors = theme.button[getVariant(otherProps)];
      return css({ ...colors.buttonEnabled, ':hover': colors.buttonBarHover });
    }};
  }
`;
