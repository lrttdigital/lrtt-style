import { css } from 'emotion';
import styled from 'react-emotion';
import * as mixins from '../mixins';

export const CheckButton = styled('button')`
  ${mixins.textFont};
  font-size: 1em;
  text-transform: uppercase;

  padding: 0.7em 0.2em;
  margin: 0.4em;
  border: 0;
  background: none;
  cursor: pointer;
  border-radius: 50%;
  height: 4em;
  width: 4em;

  user-select: none;
  -webkit-tap-highlight-color: transparent;

  transition: all 0.3s;

  ${({ selected, theme }) =>
    css({
      color: selected ? theme.checkbox.checked : theme.checkbox.unchecked,
      ':hover': { background: theme.checkbox.hoverBackground }
    })};

  &:focus {
    outline: none;
  }
`;
