import { css } from 'emotion';
import styled from 'react-emotion';

import * as mixins from '../mixins';
import { getVariant } from '../utils';

export const ButtonBase = styled('button')`
  ${mixins.textFont};
  font-size: 1em;

  padding: 0.7em 0.5em;
  margin: 0.4em;
  transition: all 0.3s;
  border: none;
  border-radius: 2px;
  white-space: nowrap;

  user-select: none;
  -webkit-tap-highlight-color: transparent;

  &:first-letter {
    text-transform: capitalize;
  }

  ${({ theme }) => css`
    &:not(:disabled) {
      &:focus {
        box-shadow: 0 0 3px 2px ${theme.colors.subtleBlue};
      }
    }
  `};

  ${({ theme, ...otherProps }) => {
    const button = theme.button[getVariant(otherProps)];

    const { color, background, selectors } = button;

    return css`
      ${css({ color, background })};
      ${Object.entries(selectors).map(([selector, values]) => {
        const key = `&:${selector}`;
        return css({ [key]: values });
      })};
    `;
  }};
`;
