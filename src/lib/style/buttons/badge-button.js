import React from 'react';
import { css } from 'emotion';
import styled from 'react-emotion';

import { RoundButton } from './round-button';
import { Badge } from '../misc';
import { Text } from '../text';

const PositionBadge = styled(Badge)`
  ${({ hide }) => css({ opacity: hide ? 0 : 1 })};
  position: absolute;
  top: 0.3em;
  right: 0.3em;

  transition: opacity 0.3s;
  pointer-events: none;
`;

const BadgeButtonContainer = styled('div')`
  position: relative;
  display: inline-block;
`;

export const BadgeButton = ({ badgeContent, hideBadge, ...otherProps }) => (
  <BadgeButtonContainer>
    <RoundButton {...otherProps} />
    <PositionBadge hide={hideBadge}>
      <Text inherit bold>
        {badgeContent}
      </Text>
    </PositionBadge>
  </BadgeButtonContainer>
);
