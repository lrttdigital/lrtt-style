import { css } from 'emotion';
import styled from 'react-emotion';

import { media } from '../../utils/styled-utils';

import { ButtonBase } from './button-base';
import { getVariant } from '../utils';

export const ButtonBar = styled('span')`
  > :first-child {
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
  }

  > :last-child {
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
  }
`;

ButtonBar.Button = styled(ButtonBase)`
  margin: 0;
  border-radius: 0;
  border: 0;
  width: 10em;

  ${({ selected, theme, ...otherProps }) => {
    const colors = theme.button[getVariant(otherProps)];

    if (selected) return css(colors);

    const { color, background } = colors.unselected;

    return css`
      ${css({ color, background })};

      &:enabled {
        cursor: pointer;

        ${css({ '&:hover': colors.buttonBarHover })};
      }
    `;
  }};

  ${media.phone`
    width: 8em;
    font-size: 1em;
  `};
`;
