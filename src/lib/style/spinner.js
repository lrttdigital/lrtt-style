import React from 'react';
import styled from 'react-emotion';

const SpacerDiv = styled('div')`
  margin: 20px;
  opacity: 0;

  @keyframes fadeIn {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }

  animation: fadeIn ease-in 1;
  animation-fill-mode: forwards;
  animation-duration: 1s;
  animation-delay: 0.25s;
`;

const SCircle = styled('circle')`
  fill: ${({ theme }) => theme.spinner.color};
`;

const SG = styled('g')`
  stroke: ${({ theme }) => theme.spinner.color};
`;

// Adapted from Sam Herbert (@sherb), for everyone. More @ http://goo.gl/7AJzbL
export const SpinningCirclesSpinner = () => (
  <SpacerDiv>
    <svg
      width="58"
      height="58"
      viewBox="0 0 58 58"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g fill="none" fillRule="evenodd">
        <SG transform="translate(2 1)" strokeWidth="1.5">
          <SCircle cx="42.601" cy="11.462" r="5" fillOpacity="1">
            <animate
              attributeName="fill-opacity"
              begin="0s"
              dur="1.3s"
              values="1;0;0;0;0;0;0;0"
              calcMode="linear"
              repeatCount="indefinite"
            />
          </SCircle>
          <SCircle cx="49.063" cy="27.063" r="5" fillOpacity="0">
            <animate
              attributeName="fill-opacity"
              begin="0s"
              dur="1.3s"
              values="0;1;0;0;0;0;0;0"
              calcMode="linear"
              repeatCount="indefinite"
            />
          </SCircle>
          <SCircle cx="42.601" cy="42.663" r="5" fillOpacity="0">
            <animate
              attributeName="fill-opacity"
              begin="0s"
              dur="1.3s"
              values="0;0;1;0;0;0;0;0"
              calcMode="linear"
              repeatCount="indefinite"
            />
          </SCircle>
          <SCircle cx="27" cy="49.125" r="5" fillOpacity="0">
            <animate
              attributeName="fill-opacity"
              begin="0s"
              dur="1.3s"
              values="0;0;0;1;0;0;0;0"
              calcMode="linear"
              repeatCount="indefinite"
            />
          </SCircle>
          <SCircle cx="11.399" cy="42.663" r="5" fillOpacity="0">
            <animate
              attributeName="fill-opacity"
              begin="0s"
              dur="1.3s"
              values="0;0;0;0;1;0;0;0"
              calcMode="linear"
              repeatCount="indefinite"
            />
          </SCircle>
          <SCircle cx="4.938" cy="27.063" r="5" fillOpacity="0">
            <animate
              attributeName="fill-opacity"
              begin="0s"
              dur="1.3s"
              values="0;0;0;0;0;1;0;0"
              calcMode="linear"
              repeatCount="indefinite"
            />
          </SCircle>
          <SCircle cx="11.399" cy="11.462" r="5" fillOpacity="0">
            <animate
              attributeName="fill-opacity"
              begin="0s"
              dur="1.3s"
              values="0;0;0;0;0;0;1;0"
              calcMode="linear"
              repeatCount="indefinite"
            />
          </SCircle>
          <SCircle cx="27" cy="5" r="5" fillOpacity="0">
            <animate
              attributeName="fill-opacity"
              begin="0s"
              dur="1.3s"
              values="0;0;0;0;0;0;0;1"
              calcMode="linear"
              repeatCount="indefinite"
            />
          </SCircle>
        </SG>
      </g>
    </svg>
  </SpacerDiv>
);
