export const getVariant = ({ secondary, accent }) => {
  if (secondary) return 'secondary';
  if (accent) return 'accent';
  return 'primary';
};
