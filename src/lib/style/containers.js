import { css } from 'emotion';
import styled from 'react-emotion';

import { getVariant } from './utils';

export const Container = styled('div')`
  ${({ inherit, theme, ...otherProps }) =>
    inherit
      ? ''
      : css({ background: theme.background[getVariant(otherProps)] })};
`;

export const HeaderArea = styled(Container)`
  width: 100%;
  z-index: 100;
  box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.2),
    0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 2px 1px -1px rgba(0, 0, 0, 0.12);
`;

export const CenterContentContainer = styled(Container)`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;
