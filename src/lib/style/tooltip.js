import React from 'react';
import { css } from 'emotion';
import styled from 'react-emotion';

import * as mixins from './mixins';

export const Hoverable = styled('span')`
  position: relative;
  display: inline-block;

  &:hover > :first-child {
    display: block;
    transform: translate(-50%) scale(1);
    opacity: 0.9;
  }
`;

const TooltipView = styled('div')`
  ${mixins.textFont};
  font-size: 0.8em;

  ${({ theme }) => css(theme.tooltip)};

  white-space: nowrap;
  display: none;
  text-align: center;
  padding: 0.3em 0.5em;
  border-radius: 6px;

  position: absolute;
  top: 100%;
  left: 50%;
  z-index: 1;
  transform: translate(-50%) scale(0);
  opacity: 0;

  transition: all 0.1s;
`;

export class Tooltip extends React.PureComponent {
  render() {
    const { tooltip, children } = this.props;

    return (
      <Hoverable>
        <TooltipView>{tooltip}</TooltipView>
        {children}
      </Hoverable>
    );
  }
}
