import styled from 'react-emotion';
import { css } from 'emotion';

import * as mixins from './mixins';

export const Badge = styled('span')`
  ${mixins.textFont};
  width: 1.7em;
  height: 1.7em;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 0;
  z-index: 1100;
  user-select: none;

  ${({ theme }) => css(theme.badge)};
`;

export const Chip = styled('span')`
  ${mixins.textFont};
  font-size: 0.8em;
  border-radius: 1em;
  padding: 0.3em 0.5em;

  ${({ theme }) => css`
    border: 1px solid ${theme.colors.deepGrey};
    color: ${theme.colors.deepGrey};
  `};
`;
