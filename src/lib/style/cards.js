import styled from 'react-emotion';

export const Card = styled('div')`
  max-width: 25em;
  color: inherit;

  border-radius: 3px;
  padding: 0.3em 1em;

  box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.2),
    0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 2px 1px -1px rgba(0, 0, 0, 0.12);

  transition: box-shadow 0.2s ease, background 1s ease, transform 0.2s ease;

  &:hover {
    box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.2),
      0px 4px 4px 0px rgba(0, 0, 0, 0.14), 0px 8px 4px -4px rgba(0, 0, 0, 0.12);
    background: #fafafa;
    transform: scale(1.05);
  }
`;
