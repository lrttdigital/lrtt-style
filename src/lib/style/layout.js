import styled from 'react-emotion';
import { css } from 'emotion';

export const Rows = styled('div')`
  display: flex;
  flex-direction: column;
  ${({ hCenter }) => (hCenter ? css`align-items: center` : '')};
  ${({ hStretch }) => (hStretch ? css`align-items: stretch` : '')};
`;

export const Columns = styled('div')`
  display: flex;
  flex-direction: row;
`;

