import React from 'react';

import { Style } from '../..';

export class DropDown extends React.PureComponent {
  render() {
    const { options = [], ...otherProps } = this.props;

    return (
      <Style.Select {...otherProps} disabled={options.length <= 0}>
        {options.map(([value, text]) => (
          <option key={value} value={value} disabled={value === ''}>
            {text}
          </option>
        ))}
      </Style.Select>
    );
  }
}
