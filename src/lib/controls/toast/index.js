import React from 'react';

import ReactDOM from 'react-dom';
import styled, { keyframes } from 'react-emotion';
import { Style, Theme } from '../..';

const fade = keyframes`
  from, to {
    transform: translate(-100%, 0);
    opacity: 0;
  }
  
  10%, 90% {
    transform: translate(0, 0);
    opacity: 1;
  }
`;

export const toast = async (
  content = 'message',
  { delay = 5000, animation = fade, wrapper = null } = {}
) => {
  const container = document.createElement('div');
  container.id = 'toast';
  document.body.appendChild(container);

  const ToastDiv = styled('div')`
    position: fixed;
    top: 10px;
    left: 10px;
    padding: 1em 1.5em;
    background: white;
    border-radius: 10px;
    z-index: 3000;
    max-width: 70%;

    animation: ${animation} ${delay}ms ease;
    box-shadow: 0 0 3px 2px lightgrey;

    &:hover {
      animation-play-state: paused;
    }
  `;

  const removeToast = () => {
    document.body.removeChild(container);
  };

  const Wrapper = wrapper;
  let fullContent = content;
  if (wrapper) fullContent = <Wrapper>{content}</Wrapper>;

  ReactDOM.render(
    <ToastDiv id="toastDiv" onClick={removeToast} onAnimationEnd={removeToast}>
      {fullContent}
    </ToastDiv>,
    container
  );
};

export const styledToast = (content = 'message', options) => {
  let styledContent = content;
  if (typeof content === 'string')
    styledContent = <Style.Text>{content}</Style.Text>;

  return toast(styledContent, {
    ...options,
    wrapper: Theme.WithLRTTTheme
  });
};
