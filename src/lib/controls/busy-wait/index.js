import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'react-emotion';
import { css } from 'emotion';
import { Style } from '../..';
import { WithLRTTTheme } from '../../lrtt-theme';

const ModalDiv = styled('div')`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  z-index: 1300;
  position: fixed;
  background: rgba(255, 255, 255, 0.8);
  display: flex;
  align-items: center;
  justify-content: center;

  @keyframes fadeIn {
    from {
      opacity: 0.2;
    }
    to {
      opacity: 1;
    }
  }

  opacity: 0.2;
  animation: fadeIn ease-in 1s;
  animation-fill-mode: forwards;
  animation-duration: 0.3s;
  ${({ delay }) => (delay ? css({ animationDelay: delay }) : '')};
`;

export class BusyWait extends React.PureComponent {
  container = document.createElement('div');

  componentDidMount() {
    document.body.appendChild(this.container);
    this.focus();
  }

  componentWillUnmount() {
    document.body.removeChild(this.container);
    const focusElements = document.getElementsByClassName('autofocus');

    if (focusElements && focusElements[0]) {
      focusElements[0].focus();
    }
  }

  focus = () => {
    const element = document.getElementById('modal');
    if (element) element.focus();
  };

  render() {
    const { ...otherProps } = this.props;

    const modalWindow = (
      <ModalDiv id="modal" tabIndex="-1" onBlur={this.focus} {...otherProps}>
        <Style.SpinningCirclesSpinner />
      </ModalDiv>
    );

    return ReactDOM.createPortal(modalWindow, this.container);
  }
}

const preventTab = evt => {
  if (evt.keyCode === 9) evt.preventDefault();
};

export const busyWait = async f => {
  const container = document.createElement('div');
  container.id = 'modal';
  document.body.appendChild(container);
  ReactDOM.render(
    <ModalDiv id="busyWait" tabIndex="-1" onKeyDown={preventTab}>
      <WithLRTTTheme>
        <Style.SpinningCirclesSpinner />
      </WithLRTTTheme>
    </ModalDiv>,
    container
  );

  const element = document.getElementById('busyWait');

  if (element) element.focus();

  try {
    await f();
  } catch (err) {
    document.body.removeChild(container);
    throw err;
  }

  document.body.removeChild(container);
};
