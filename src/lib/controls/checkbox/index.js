import React from 'react';
import { Style } from '../..';

export class Checkbox extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      checked: props.value || false
    };
  }

  get value() {
    return this.state.checked;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.value !== this.props.value) {
      this.setState({ checked: this.props.value }, this.propagateChange);
    }
  }

  propagateChange = () => {
    const { onChange } = this.props;

    if (onChange) onChange({ target: { value: this.value } });
  };

  toggle = () => {
    if (!this.props.disabled) {
      this.setState(
        ({ checked }) => ({ checked: !checked }),
        this.propagateChange
      );
    }
  };

  render() {
    return (
      <Style.Checkbox
        {...this.props}
        checked={this.state.checked}
        onClick={this.toggle}
        onKeyUp={evt => {
          if (evt.keyCode === 13) this.toggle();
        }}
      />
    );
  }
}
