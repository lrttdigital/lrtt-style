import styled from 'react-emotion';

export const SelectorItemRows = styled('div')`
  width: 100%;

  > :not(:first-child) {
    margin-top: 0.6em;
  }
  > :not(:last-child) {
    margin-bottom: 0.6em;
  }
`;
