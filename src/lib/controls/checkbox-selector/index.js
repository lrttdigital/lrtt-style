import React from 'react';

import { Style } from '../..';
import { SelectorItemRows } from '../layout';

export class CheckboxSelector extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      valueSet: props.value || new Set()
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.value !== this.props.value) {
      this.setState({ valueSet: this.props.value }, this.propagateChange);
    }
  }

  propagateChange = () => {
    const { onChange } = this.props;
    if (onChange) {
      onChange({ target: { value: this.valueSet } });
    }
  };

  toggleSelection = (value, max) => {
    this.setState(({ valueSet }) => {
      const newSet = new Set(valueSet);

      if (newSet.has(value)) newSet.delete(value);
      else {
        if (newSet.size < max) {
          newSet.add(value);
        }
      }

      return { valueSet: newSet };
    }, this.propagateChange);
  };

  get valueSet() {
    return this.state.valueSet;
  }

  handleKeyUp = value => evt => {
    if (evt.keyCode === 13) this.toggleSelection(value);
  };

  render() {
    const { options, type, max, ...otherProps } = this.props;
    const { valueSet } = this;

    const elements = options.map(
      ([value, text]) =>
        type === 'checkbox' ? (
          <Style.Checkbox
            key={value}
            onClick={() => this.toggleSelection(value, max)}
            text={text}
            checked={valueSet.has(value)}
            onKeyUp={this.handleKeyUp(value)}
          />
        ) : (
          <Style.Radio
            key={value}
            onClick={() => this.toggleSelection(value, max)}
            text={text}
            checked={valueSet.has(value)}
            onKeyUp={this.handleKeyUp(value)}
          />
        )
    );

    return <SelectorItemRows {...otherProps}>{elements}</SelectorItemRows>;
  }
}
