import React from 'react';
import ReactDOM from 'react-dom';
import { css } from 'emotion';
import styled from 'react-emotion';

const ModalDiv = styled('div')`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  z-index: 1300;
  position: fixed;
  background: rgba(0, 0, 0, 0.7);

  @keyframes fadeIn {
    from {
      opacity: 0.2;
    }
    to {
      opacity: 1;
    }
  }

  opacity: 0.2;
  animation: fadeIn ease-in 1s;
  animation-fill-mode: forwards;
  animation-duration: 0.3s;
  ${({ delay }) => (delay ? css({ animationDelay: delay }) : '')};
`;

class ModalWindow extends React.PureComponent {
  container = document.createElement('div');

  componentWillMount() {
    document.body.appendChild(this.container);
  }

  componentDidMount() {
    document.getElementById('modal').focus();
  }

  componentWillUnmount() {
    document.body.removeChild(this.container);
  }

  render() {
    const { children, onClose, ...otherProps } = this.props;

    const modalWindow = (
      <ModalDiv
        id="modal"
        tabIndex="-1"
        onClick={onClose}
        onKeyDown={onClose}
        {...otherProps}
      >
        {children}
      </ModalDiv>
    );

    return ReactDOM.createPortal(modalWindow, this.container);
  }
}

export class SuperModal extends React.PureComponent {
  state = { open: false };

  open = () => this.setState({ open: true });

  handleClose = evt => {
    evt.preventDefault();
    evt.stopPropagation();
    evt.nativeEvent.stopImmediatePropagation();
    this.setState({ open: false });
  };

  render() {
    const {
      render: View,
      content: Content,
      renderProps,
      contentProps,
      ...otherProps
    } = this.props;

    return (
      <React.Fragment>
        <View onOpen={this.open} {...renderProps} />
        {this.state.open && (
          <ModalWindow onClose={e => this.handleClose(e)} {...otherProps}>
            <Content {...contentProps} />
          </ModalWindow>
        )}
      </React.Fragment>
    );
  }
}
