import React from 'react';

import { Style } from '../..';
import { SelectorItemRows } from '../layout';

export class RadioSelector extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      value: props.value || ''
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.value !== this.props.value) {
      this.setState({ value: this.props.value }, this.propagateChange);
    }
  }

  propagateChange = () => {
    const { onChange } = this.props;

    if (onChange) {
      // simulate input element
      onChange({ target: { value: this.value } });
    }
  };

  setSelection = value => {
    this.setState({ value }, this.propagateChange);
  };

  get value() {
    return this.state.value;
  }

  handleKeyUp = value => evt => {
    if (evt.keyCode === 13) this.setSelection(value);
  };

  render() {
    const { options, inputId } = this.props;

    const elements = [];
    options.map(([value, text], index) =>
      elements.push(
        <Style.Radio
          id={index === 0 ? inputId : null}
          key={value}
          onClick={() => this.setSelection(value)}
          text={text}
          checked={value === this.value}
          onKeyUp={this.handleKeyUp(value)}
        />
      )
    );

    return <SelectorItemRows>{elements}</SelectorItemRows>;
  }
}
