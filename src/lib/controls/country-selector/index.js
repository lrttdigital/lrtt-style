import React from 'react';
import { Controls } from '../..';

// TODO should be code split but can't get webpack to load correct chunk
import { countryList } from './countryList';

export class CountrySelector extends React.PureComponent {
  state = {
    countryOptions: [['', 'Please select a country..']]
  };

  loadOptions = async () => {
    // TODO
    // const { countryList } = await import(/* webpackChunkName: "countryList" */ './countryList');

    this.setState(({ countryOptions }) => ({
      countryOptions: [...countryOptions, ...countryList]
    }));
  };

  componentDidMount() {
    this.loadOptions();
  }

  render() {
    return (
      <Controls.DropDown
        id={this.props.inputId}
        options={this.state.countryOptions}
        {...this.props}
      />
    );
  }
}
