import React from 'react';
import styled from 'react-emotion';

import { RoundButton, Chip, TextInput } from '../../style';

const ChipList = styled('div')`
  display: flex;
  flex-wrap: wrap;

  > * {
    margin-right: 0.3em;
    margin-top: 0.5em;

    @keyframes fadeIn {
      from {
        opacity: 0;
      }
      to {
        opacity: 1;
      }
    }

    animation: fadeIn 0.3s;
  }
`;

const OverlayContainer = styled('div')`
  position: relative;
`;

const AddButton = styled(RoundButton)`
  position: absolute;
  padding: 0;
  margin: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 0;
  top: 0;
  right: 0;
  height: 100%;
  width: 4em;
`;

export class ChipInput extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      input: '',
      values: props.value || []
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.value !== this.props.value) {
      this.setState({ values: this.props.value }, this.propagateChange);
    }
  }

  get values() {
    return this.state.values;
  }

  propagateChange = () => {
    const { onChange } = this.props;

    if (onChange) {
      onChange({ target: { value: this.values } });
    }
  };

  handleChange = evt => {
    this.setState({ input: evt.target.value });

    if(evt.target.value === ',') {
      this.setState({ input: '' });
      this.propagateChange
    }
  };

  get canSubmit() {
    const trimInput = this.state.input.trim();

    return trimInput.length >= 2;
  }

  handleKeyDown = key => {
    if ((key.keyCode === 13 || key.keyCode === 188) && this.canSubmit) this.handleSubmit();
  };

  handleRemove = index => {
    this.setState(({ values: oldValues }) => {
      const values = [...oldValues];
      values.splice(index, 1);

      return { values };
    }, this.propagateChange);
  };

  handleSubmit = () => {
    const trimInput = this.state.input.trim();

    this.setState(
      ({ input, values }) => ({
        input: '',
        values: [...values, trimInput]
      }),
      this.propagateChange
    );
  };

  render() {
    const { input } = this.state;
    const { inputProps, inputId } = this.props;

    return (
      <React.Fragment>
        <OverlayContainer>
          <TextInput
            id={inputId}
            value={input}
            onChange={this.handleChange}
            onKeyDown={this.handleKeyDown}
            style={{ width: '100%', boxSizing: 'border-box' }}
            {...inputProps}
          />
          <AddButton disabled={!this.canSubmit} onClick={this.handleSubmit}>
            Add
          </AddButton>
        </OverlayContainer>

        <ChipList>
          {this.state.values.map((value, index) => (
            <Chip key={index}>
              {value}
              <i
                onClick={() => this.handleRemove(index)}
                className="fas fa-times"
                style={{ marginLeft: '0.3em', cursor: 'pointer' }}
              />
            </Chip>
          ))}
        </ChipList>
      </React.Fragment>
    );
  }
}
