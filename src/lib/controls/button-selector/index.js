import React from 'react';
import { Style } from '../..';

export class ButtonSelector extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      value: props.value || ''
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.value !== this.props.value) {
      this.setState({ value: this.props.value }, this.propagateChange);
    }
  }

  propagateChange = () => {
    const { onChange } = this.props;

    if (onChange) {
      // simulate input element
      onChange({ target: { value: this.value } });
    }
  };

  setSelection = value => {
    this.setState({ value }, this.propagateChange);
  };

  getClass = value => {
    if(value) {
      return "button active";
    }
    else {
      return "button";
    }
  }

  get value() {
    return this.state.value;
  }

  render() {
    const { options, inputId } = this.props;

    return (
      <Style.ButtonSelector>
        {options.map(([key, text], index) => (
          <Style.ButtonSelector.Button
            id={index === 0 ? inputId : null}
            key={key}
            onClick={() => this.setSelection(key)}
            selected={key === this.value}
            className={this.getClass(key === this.value)}
          >
            {text}
          </Style.ButtonSelector.Button>
        ))}
      </Style.ButtonSelector>
    );
  }
}
