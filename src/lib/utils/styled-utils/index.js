import React from 'react';
import { css } from 'emotion';

export class Props {
  static unpack = selector => (...input) => props =>
    Props.withContext({ ...Props.select(selector)(props), ...props })(...input);

  static withProps = getProps => Comp => props => {
    if (typeof getProps === 'function')
      return <Comp {...props} {...getProps(props)} />;

    return <Comp {...props} {...getProps} />;
  };
}

const sizes = {
  desktop: 992,
  tablet: 768,
  phone: 576
};

// Iterate through the sizes and create a media template
export const media = Object.keys(sizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (max-width: ${sizes[label] / 16}em) {
      ${css(...args)};
    }
  `;

  return acc;
}, {});

export class Props2 {
  static when = value => rest => (value ? rest : '');

  static assign = (cssName, value) => rest =>
    value ? `${cssName}: ${value}; ${rest}` : rest;

  static assignAll = values => rest => {
    let result = '';

    Object.entries(values).forEach(
      ([key, value]) => (result += `${key}: ${value}; `)
    );

    return `${result} ${rest}`;
  };
}
