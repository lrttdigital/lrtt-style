import React from 'react';
import { Style, Controls } from '../lib';
import { WithLRTTTheme } from '../lib/lrtt-theme';

import styled from 'react-emotion';

const SpacerDiv = styled('div')`
  > * {
    margin-bottom: 2em;
  }
`;

const wait = async () => {
  Controls.busyWait(() => new Promise(resolve => setTimeout(resolve, 10000)));
};

const App = () => (
  <div>
    <WithLRTTTheme>
      <SpacerDiv>
        <Style.Button>Hello</Style.Button>
        <Style.Radio text="Hello" />
        <Style.Radio text="Hello" checked />
        <Style.Input />
        <Style.DateInput />
        <Style.Chip>Hello</Style.Chip>
        <Style.Checkbox text="This is a checkbox option with text that should wrap at some point" />
        <Style.Checkbox
          text="This is a checkbox option with text that should wrap at some point"
          checked
        />
        <Style.CheckboxInput />
        <Style.Button onClick={wait}>BusyWait</Style.Button>
        <Style.Button onClick={() => Controls.styledToast("I'm a toast!")}>
          Toast
        </Style.Button>
        <Controls.CountrySelector />
        <Style.DateInput />
      </SpacerDiv>
    </WithLRTTTheme>
  </div>
);

export default App;
